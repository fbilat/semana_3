// iniciamos popover y carousel y mas
$(function () {
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
        interval: 2000
    });
    // mostrandose y editar class
    $('#contactoModal').on('show.bs.modal', function (e) {
        console.log('Ventana modal del registro del boletin, mostrandose');

        $('#boletin-btn').removeClass('bg-info');
        $('#boletin-btn').addClass('btn-secondary');
        // desabilita el boton
        $('#boletin-btn').prop('disabled', true);
    });
    // mostró
    $('#contactoModal').on('shown.bs.modal', function (e) {
        console.log('Ventana modal del registro del boletin, se mostró');
    });
    // se oculta
    $('#contactoModal').on('hide.bs.modal', function (e) {
        console.log('Ventana modal del registro del boletin, se oculta');
    });
    //se ocultó y restauramos el boton a su estado original
    $('#contactoModal').on('hidden.bs.modal', function (e) {
        console.log('Ventana modal del registro del boletin, se ocultó');
        $('#boletin-btn').removeClass('btn-secondary');
        $('#boletin-btn').addClass('bg-info');
        $('#boletin-btn').prop('disabled', false);
    });

})