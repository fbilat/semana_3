module.exports = function(grunt){
    // precarga modulos de grunt
    require('time-grunt')(grunt);
    require('jit-grunt')(grunt,{
        useminPrepare: 'grunt-usemin'
    })
    // configuracion inicial
    grunt.initConfig({
        //sass
        sass:{
            dist:{
                files:[{
                    expand: true,
                    cwd: 'styles',
                    src: ['*.scss'],
                    dest: 'css',
                    ext: '.css'
                }]
            }
        },
        //watch
        watch:{
            files: ['css/*.scss'],
            tasks: ['css']
        },
        //Sincronizacion broswer
        browserSync:{
            dev:{
                bsfiles: { // browser files
                    src:[
                        'css/*.css',
                        '*.html',
                        'js/*.js'
                    ]
                },
                options:{
                    watchTask: true,
                    server:{
                        basedir: './' //directorio base del servidor
                    }
                }
            }
        },
        // imagemin
        imagemin:{
            dynamic:{
                files:[{
                    expand: true,
                    cwd: './',
                    src: 'img/*.{png,gif,jpg,jpeg}',
                    dest: 'dist/',
                }]
            }
        },
        // monitorea la carga de los modulos (en tiempo) - TIME-GRUNT

        // copia de archivos - COPY
        copy:{
            html:{
                files: [{
                    expand: true,
                    dot: true,
                    cwd: './',
                    src: ['*.html'],
                    dest: 'dist'
                }],
            },
            fonts:{
                files:[{ 
                    expand: true,
                    dot: true,
                    cwd: 'mode_modules/open-iconic/font',
                    src: ['fonts/*.*'],
                    dest: 'dist'
                }]
            }
        },
        // borrado de directorio -- grunt-contrib-clean
        clean:{
            build:{
                src:['dist/']
            }
        },
        // miniminiza css -- cssmin
        cssmin:{
            dist:{}
        },
        
        // minimiza js -- uglify
        uglify:{
            dist: {}
        },
        // versionado de archivos -- grunt-filerev
        filerev:{
            options:{
                encoding: 'utf8',
                algorithm: 'md5',
                length: 20
            },
            release: {
                files: [{
                    src: [
                        'dist/js/*.js',
                        'dist/css/*.css',
                    ]
                }]
            }
        },
         // concatena archivos -- grunt-contrib-concat
         concat:{
             options:{
                 separator: ';',
             }
         },
        // minimiza archivos -- grunt-usemin
        useminPrepare:{
            foo:{
                dest: 'dist',
                src: ['index.html','nosotros.html','contacto.html','precios.html','terminos.html']
            }
        },
        options:{
            flow:{
                steps:{
                    css:['cssmin'],
                    js:['uglity']
                }
            }
        },
        post:{
            css:[{
                name: 'cssmin',
                createConfig: function(context, block){
                    var generated = context.options.generated;
                    generated.options = {
                        keepSpecialComments: 0,
                        rebase: false
                    }
                }
            }]
        },
        usemin: {
            html:['dist/index.html','dist/contacto.html','dist/nosotros.html','dist/precios.html','dist/terminos.html'],
        },
        options:{
            assetsDir: ['dist','dist/css','dist/js']
        }






    });

    // cargamos modulos
    // grunt.loadNpmTasks('grunt-contrib-sass');
    // grunt.loadNpmTasks('grunt-contrib-watch');
    // grunt.loadNpmTasks('grunt-browser-sync');
    //grunt.loadNpmTasks('grunt-contrib-imagemin');

    // registramos la tarea
    grunt.registerTask('css',['sass']);
    grunt.registerTask('default',['browserSync','watch']);
    grunt.registerTask('img:compress',['imagemin']);
    grunt.registerTask('build',[
        'clean',
        'copy',
        'imagemin',
        'useminPrepare',
        'concat',
        'cssmin',
        'uglify',
        'filerev',
        'usemin'
    ])
 
};